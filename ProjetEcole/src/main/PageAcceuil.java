package main;

import net.proteanit.sql.DbUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.Objects;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PageAcceuil extends JFrame {
    private JPanel Acceuil;
    //menu de l'application
    private JTabbedPane Header;
    //Menu Départements
    //champ nom
    private JTextField textField1;
    //champ chef
    private JTextField textField2;
    //champ recherche
    private JTextField textField3;
    //bouttons
    private JButton ajouterButton;
    private JButton modifierButton;
    private JButton supprimerButton;
    private JButton rechercherButton;
    //tableau des Départements
    private JTable table1;
    //Menu options
    //champ nom
    private JTextField Nom_option;
    //champ Départements
    private JComboBox<String> comboBox_Depart_in_option ;
    //champ rechercher
    private JTextField rechercher_field;
    //bouttons
    private JButton ajouterButton_option;
    private JButton modifierButton_option;
    private JButton supprimerButton_option;
    private JButton rechercherButton_option;
    //tableau des options
    private JTable table3;
    //Menu elements
    //champ nom
    private JTextField textField6;
    //champ coef
    private JTextField textField7;
    //champ options
    private JComboBox<String> comboBox2;
    //champ rechercher
    private JTextField textField8;
    //bouttons
    private JButton ajouterButton2;
    private JButton modifierButton2;
    private JButton supprimerButton2;
    private JButton rechercherButton2;
    //tableau des elements
    private JTable table2;
    //Menu Professeurs
    //champ nom
    private JTextField textField9;
    //champ tel
    private JTextField textField10;
    //champ elemnt
    private JComboBox<String> comboBox3;
    //champ email
    private JTextField textField11;
    //champ rechercher
    private JTextField textField12;
    //bouttons
    private JButton ajouterButton3;
    private JButton modifierButton3;
    private JButton supprimerButton3;
    private JButton rechercherButton3;
    //tableau des prof
    private JTable table4;
    //Menu etudiants
    //champ nom
    private JTextField textField13;
    //champ tel
    private JTextField textField14;
    //champ email
    private JTextField textField15;
    //champ options
    private JComboBox<String> comboBox4;
    //champ rechercher
    private JTextField textField16;
    //bouttons
    private JButton ajouterButton4;
    private JButton modifierButton4;
    private JButton supprimerButton4;
    private JButton rechercherButton5;
    //tableau des etudiants
    private JTable table5;
    //Menu notes
    //champ etudiant
    private JComboBox<String> comboBox5;
    //champ element
    private JComboBox<String> comboBox6;
    //champ note
    private JTextField textField20;
    //champs dateEva
    private JTextField jour;
    private JTextField mois;
    private JTextField annee;
    //champ rechercher
    private JTextField textField21;
    //bouttons
    private JButton ajouterButton5;
    private JButton modifierButton5;
    private JButton supprimerButton5;
    private JButton rechercherButton4;

    private JTable table6;
    //boutton actualiser
    private JButton actualiserButton;
    //boutton de deconnexion
    private JButton deconnectionButton;
    private JLabel act;
    private JLabel deco;
    private JButton parcourirButton;
    private JLabel imglabel;
    //champs de connectivité
    Connection con;
    Statement st;
    ResultSet res;
    //valeur etudiant selectionnée
    String selectedValue;
    // path
    URL pathurl = ClassLoader.getSystemResource("avatar-1577909_960_720.png");
    private String path = null;
    public URL pathicone = ClassLoader.getSystemResource("icone.png");

    public PageAcceuil(String titre) {
        super(titre);
        Connect();//connaction a da base de données
        //definition de la taille de la fenetre
        this.setSize(1000, 472);
        //definition de la position de la fenetre
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(Acceuil);
        this.pack();
        try {
            Image img = ImageIO.read(pathicone);
            this.setIconImage(img);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //chargement de tableaux
        table_load();
        //chargement des JComboBox
        field_load();
        //boutton deconnection
        deconnectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                dispose();
                JFrame frame = new PageConnexion("Connexion");
                frame.setVisible(true);
            }
        });
        //boutton actualiser
        actualiserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table_load();
                field_load();
            }
        });

        //------------------------------Départements------------------------------
        //ajouter Départements
        ajouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterdept();
                field_load();
            }
        });
        //modifier Départements
        modifierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifierdept();
                field_load();
            }
        });
        //supprimer Départements
        supprimerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimerdept();
                field_load();
            }
        });
        //rechercher Départements
        rechercherButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chercherdept();
            }
        });
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table1.getSelectedRow();
                    String id =table1.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String requete ="Select id,nom,chef from deppartement where id= '"+id+"';";
                    res= st.executeQuery(requete);
                    if(res.next()){
                        textField1.setText(res.getString("nom"));
                        textField2.setText(res.getString("chef"));
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        //----------------------------Options--------------------------------
        ajouterButton_option.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                get_option_elem();
                ajouterOpt();
                field_load();
            }
        });
        //modifier options
        modifierButton_option.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                get_option_elem();
                modifieropt();
                field_load();
            }
        });
        //supprimer option
        supprimerButton_option.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                get_option_elem();
                supprimeropt();
                field_load();
            }
        });
        //rechercher options
        rechercherButton_option.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chercheropt();
                field_load();
            }
        });
        //onclick sur la table des options
        table3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table3.getSelectedRow();
                    String id =table3.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String requete ="select options.id,options.nom,deppartement.nom,deppartement.id  from options,deppartement where deppartement.id=options.deppartement and options.id='"+id+"';";
                    res= st.executeQuery(requete);
                    if(res.next()){
                        Nom_option.setText(res.getString("nom"));
                        comboBox_Depart_in_option.getModel().setSelectedItem(res.getString("deppartement.id") + " " + res.getString("deppartement.nom"));
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }

            }
        });

        //-----------------------------elements---------------------------------
        // pour ajouter elements
        ajouterButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterelem();
                field_load();
            }
        });
        // pour modifier l'element
        modifierButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifierelem();
                field_load();
            }
        });
        //pour supprimer l'element
        supprimerButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimerelem();
                field_load();
            }
        });
        // pour rechercher l'element
        rechercherButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chercherelem();
                field_load();
            }
        });
        //onclick sur la table des elements
        table2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table2.getSelectedRow();
                    String id =table2.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String requete ="SELECT element.nom ,element.coef,element.options,options.nom,options.id FROM element,options  WHERE element.options=options.id and element.id='"+id+"'";;
                    res= st.executeQuery(requete);
                    if(res.next()){
                        textField6.setText(res.getString("element.nom"));
                        textField7.setText(res.getString("element.coef"));
                        comboBox2.getModel().setSelectedItem(res.getString("options.id")+" "+res.getString("options.nom"));
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        //--------------------------------professeurs--------------------------------
        //pour ajouter un prof
        ajouterButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterprof();
                field_load();
            }
        });
        //pour modifier un prof
        modifierButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifierprof();
                field_load();
            }
        });
        // pour supprimer un prof
        supprimerButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimerprof();
                field_load();
            }
        });
        // pour chercher un prof
        rechercherButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chercherprof();
                field_load();
            }
        });
        //onclick sur la table des profs
        table4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table4.getSelectedRow();
                    String id =table4.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String requete ="SELECT p.id,e.id, e.nom as 'element',p.Email,p.nom,p.telephone FROM professeur p ,element e WHERE e.id=p.codeElem and p.id='"+id+"';";
                    res= st.executeQuery(requete);
                    if(res.next()){
                        textField9.setText(res.getString("nom"));
                        textField10.setText(res.getString("telephone"));
                        textField11.setText(res.getString("Email"));
                        comboBox3.getModel().setSelectedItem(res.getString("e.id")+" "+res.getString("element"));
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        //------------------------------------Etudiants--------------------------------------
        //pour ajouter un etudiant
        ajouterButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouteretud();
                field_load();
            }
        });
        //pour modifier un etudiant
        modifierButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifieretud();
                field_load();
            }
        });
        //pour supprimer un etudiant
        supprimerButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimeretud();
                field_load();
            }
        });
        //pour chercher un etudiant
        rechercherButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chercheretud();
                field_load();
            }
        });
        // pour parcourir le path de l'image
        parcourirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                path = null;
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.IMAGE", "jpg","gif","png");
                fileChooser.addChoosableFileFilter(filter);
                int result = fileChooser.showSaveDialog(null);
                if(result == JFileChooser.APPROVE_OPTION){
                    File selectedFile = fileChooser.getSelectedFile();
                    path = selectedFile.getAbsolutePath();
                    imglabel.setIcon(new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT)));
                }
                else if(result == JFileChooser.CANCEL_OPTION){
                    System.out.println("No Data");}
            }
        });
        //onclick sur la table des etudiants
        table5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table5.getSelectedRow();
                    String id =table5.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String requete ="Select etudiant.id,etudiant.img,etudiant.nom,etudiant.telephone,etudiant.Email,etudiant.options,options.id,options.nom from etudiant,options where etudiant.id= '"+id+"' AND etudiant.options=options.id;";
                    res= st.executeQuery(requete);
                    if(res.next()){
                        textField13.setText(res.getString("etudiant.nom"));
                        textField14.setText(res.getString("etudiant.telephone"));
                        textField15.setText(res.getString("etudiant.Email"));
                        comboBox4.getModel().setSelectedItem(res.getString("options.id")+" "+res.getString("options.nom"));
                        InputStream is = res.getBinaryStream(2);
                        BufferedImage bufImg = null;
                        bufImg = ImageIO.read(is);
                        Image image = bufImg;
                        ImageIcon icon =new ImageIcon(image.getScaledInstance(50, 50, Image.SCALE_DEFAULT));
                        imglabel.setIcon(icon);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }

            }
        });

        //--------------------------------------Notes--------------------------------------
        //pour ajouter une note
        ajouterButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouternote();
                field_load();
            }
        });
        //pour modifier une note
        modifierButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifiernote();
                field_load();
            }
        });
        //pour supprimer une note
        supprimerButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimernote();
                field_load();
            }
        });
        //pour chercher une note
        rechercherButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cherchernote();
                field_load();
            }
        });
        //onclick sur la table des notes
        table6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    int selectedRowIndex = table6.getSelectedRow();
                    String codeElem =table6.getModel().getValueAt(selectedRowIndex, 0).toString();
                    String codeEtud =table6.getModel().getValueAt(selectedRowIndex, 2).toString();
                    String requete ="Select codeElem,element.nom,codeEtud,etudiant.nom,note,dateEva,etudiant.id,element.id from evaluer,element,etudiant where codeElem= '"+codeElem+"' and codeEtud= '"+codeEtud+"' and element.id=codeElem and etudiant.id=codeEtud ;";
                    res= st.executeQuery(requete);
                    if(res.next()){
                        String[] words=res.getString("dateEva").substring(0,10).split("-");
                        jour.setText(words[2]);
                        mois.setText(words[1]);
                        annee.setText(words[0]);
                        comboBox5.getModel().setSelectedItem(res.getString("etudiant.id") + " " + res.getString("etudiant.nom"));
                        comboBox6.getModel().setSelectedItem(res.getString("element.id")+ " " + res.getString("element.nom"));
                        textField20.setText(res.getString("note"));
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
        comboBox5.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try{
                    selectedValue=comboBox5.getModel().getSelectedItem().toString();
                    get_elem_note();
                }
                catch (java.lang.NullPointerException j){

                }

            }
        });
    }
    //connection a la base de données
    public void Connect(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/PROJET_ECOLE_JAVA_SQL", PageConnexion.getNom(),PageConnexion.getMdp());
            st = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    //actualiser les chmanps
    void field_load(){
        path = null;
        textField1.setText("");
        textField2.setText("");
        textField3.setText("");
        textField6.setText("");
        textField7.setText("");
        textField8.setText("");
        textField9.setText("");
        textField10.setText("");
        textField11.setText("");
        textField12.setText("");
        textField13.setText("");
        textField14.setText("");
        textField15.setText("");
        textField16.setText("");
        textField20.setText("");
        jour.setText("");
        mois.setText("");
        annee.setText("");
        Nom_option.setText("");
        rechercher_field.setText("");
        get_depart();
        get_option_elem();
        get_elem_prof();
        get_opt_etud();
        get_elem_note();
        get_etud_note();
        imglabel.setIcon(null);
    }
    //charger les tableaux
    void table_load(){
        try
        {
            //Départements
            Statement st1;
            st1 = con.prepareStatement("select * from deppartement;");
            ResultSet rs1 = st1.executeQuery("select * from deppartement;");
            table1.setModel(DbUtils.resultSetToTableModel(rs1));
            //elements
            Statement st2;
            st2 = con.prepareStatement("SELECT e.id ,e.nom ,e.coef ,o.nom as 'option' FROM element e,options o  WHERE e.options=o.id order by e.id;" );
            ResultSet rs2 = st2.executeQuery("SELECT e.id ,e.nom ,e.coef ,o.nom as 'option' FROM element e,options o  WHERE e.options=o.id order by e.id;");
            table2.setModel(DbUtils.resultSetToTableModel(rs2));
            //options
            Statement st3;
            st3 = con.prepareStatement("select o.id,o.nom,d.nom as Deppartement from options o ,deppartement d where d.id=o.deppartement order by o.id;");
            ResultSet rs3 = st3.executeQuery("select o.id,o.nom,d.nom as Deppartement from options o ,deppartement d where d.id=o.deppartement order by o.id;");
            table3.setModel(DbUtils.resultSetToTableModel(rs3));
            //professeurs
            Statement st4;
            st4 = con.prepareStatement("SELECT p.id, e.nom as 'element',p.Email,p.nom,p.telephone FROM professeur p ,element e WHERE e.id=p.codeElem order by p.id;");
            ResultSet rs4 = st4.executeQuery("SELECT p.id, e.nom as 'element',p.Email,p.nom,p.telephone FROM professeur p ,element e WHERE e.id=p.codeElem order by p.id;");
            table4.setModel(DbUtils.resultSetToTableModel(rs4));
            //etudiants
            Statement st5;
            st5 = con.prepareStatement("select * from etudiant;");
            ResultSet rs5 = st5.executeQuery("select * from etudiant;");
            table5.setModel(DbUtils.resultSetToTableModel(rs5));
            //avaluer
            Statement st6;
            st6 = con.prepareStatement("select codeElem,element.nom,codeEtud,etudiant.nom,note,dateEva from evaluer,element,etudiant where etudiant.id=codeEtud AND element.id=codeElem ;");
            ResultSet rs6 = st6.executeQuery("select codeElem,element.nom,codeEtud,etudiant.nom,note,dateEva from evaluer,element,etudiant where etudiant.id=codeEtud AND element.id=codeElem ;");
            table6.setModel(DbUtils.resultSetToTableModel(rs6));
        }
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    //-----------------------------------------Départements----------------------------------------------
    public void ajouterdept(){
        try {
            res =st.executeQuery("select max(id) from deppartement;");
            System.out.println(res.next());
            String requette="insert into deppartement (id,nom,chef) values('"+(Integer.parseInt(res.getString(1))+1)+"','"+textField1.getText()+"','"+textField2.getText()+"')";
            st.executeUpdate(requette);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            textField1.setText("");
            textField2.setText("");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void supprimerdept(){
        int row = table1.getSelectedRow();
        String eve = table1.getModel().getValueAt(row, 0).toString();
        String requette ="delete from `deppartement` where id = '"+eve+"';";
        try {
            st.execute(requette);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void modifierdept(){
        int row = table1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) table1.getModel();
        String eve =  model.getValueAt(row, 0).toString();
        String requette ="update `deppartement` set nom = '"+textField1.getText()+"',chef = '"+textField2.getText()+"' where id = '"+eve+"';";
        try {
            st.execute(requette);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void chercherdept(){
        try {
            String requette ="select * from `deppartement` where nom like '%" + textField3.getText() + "%' OR id like '%"+ textField3.getText() +"%' OR chef like '%"+ textField3.getText() +"%';";
            st.execute(requette);
            res = st.executeQuery(requette);
            table1.setModel(DbUtils.resultSetToTableModel(res));
            textField3.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    //-----------------------------------------Options----------------------------------------------
    public void ajouterOpt(){
        try {
            String iddept = Objects.requireNonNull(comboBox_Depart_in_option.getSelectedItem()).toString();
            iddept= iddept.substring(0,iddept.indexOf(' '));
            res =st.executeQuery("select max(id) from options;");
            res.next();
            int id= res.getInt(1)+1;
            String requette="insert into `options` (id,nom,deppartement) values('"+id+"','"+Nom_option.getText()+"','"+iddept+"')";
            st.executeUpdate(requette);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            Nom_option.setText("");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_depart(){
        try {
            comboBox_Depart_in_option.removeAllItems();
            comboBox_Depart_in_option.setEditable(false);
            String requette ="select id,nom from deppartement;";
            ResultSet res_dept_opt;
            Statement dep =con.createStatement();
            res_dept_opt = dep.executeQuery(requette);
            while(res_dept_opt.next()){
                comboBox_Depart_in_option.addItem(res_dept_opt.getString(1) + " " +res_dept_opt.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void modifieropt(){
        String iddept = Objects.requireNonNull(comboBox_Depart_in_option.getSelectedItem()).toString();
        iddept= iddept.substring(0,iddept.indexOf(' '));

        try {
            Statement s=con.createStatement();
            int row = table3.getSelectedRow();
            DefaultTableModel model = (DefaultTableModel) table3.getModel();
            String eve3 =  model.getValueAt(row, 0).toString();
            String sql ="update `options` set nom = '"+Nom_option.getText()+"',deppartement = '"+ iddept +"' where id = '"+eve3+"';";
            st.execute(sql);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void supprimeropt(){
        int row = table3.getSelectedRow();
        String eve = table3.getModel().getValueAt(row, 0).toString();
        String requette ="delete from `options` where id = '"+eve+"';";
        try {
            st.execute(requette);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void chercheropt(){
        try {
            String sql3 ="select * from `options` where nom like '%" + rechercher_field.getText()+"%' OR id like '%" + rechercher_field.getText()+"%' ;";
            st.execute(sql3);
            res = st.executeQuery(sql3);
            table3.setModel(DbUtils.resultSetToTableModel(res));
            rechercher_field.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    //-----------------------------------------Elements----------------------------------------------
    public void ajouterelem(){
        try {
            String idopt = Objects.requireNonNull(comboBox2.getSelectedItem()).toString();
            idopt= idopt.substring(0, idopt.indexOf(' '));
            res =st.executeQuery("select max(id) from element;");
            System.out.println(res.next());
            String sqlelem="insert into `element` (id,nom,coef,options) values('"+(Integer.parseInt(res.getString(1))+1)+"','"+textField6.getText()+"','"+textField7.getText()+"','"+idopt+"')";
            st.executeUpdate(sqlelem);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            textField6.setText("");
            textField7.setText("");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_option_elem(){
        try {
            comboBox2.removeAllItems();
            comboBox2.setEditable(false);
            String sqlgetopt ="select id,nom from options;";
            res= null ;
            Statement dep =con.createStatement();
            res = dep.executeQuery(sqlgetopt);
            while(res.next()){
                comboBox2.addItem(res.getString(1)+ " " + res.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void modifierelem(){
        try {
            String idopt = Objects.requireNonNull(comboBox2.getSelectedItem()).toString();
            idopt= idopt.substring(0, idopt.indexOf(' '));
            int row = table2.getSelectedRow();
            Statement s=con.createStatement();
            DefaultTableModel model = (DefaultTableModel) table2.getModel();
            String eve3 =  model.getValueAt(row, 0).toString();
            String sql ="update `element` set nom = '"+textField6.getText()+"',coef = '"+textField7.getText()+"',options = '"+ idopt+"' where id = '"+eve3+"';";
            st.execute(sql);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void supprimerelem(){
        int row = table2.getSelectedRow();
        String eve = table2.getModel().getValueAt(row, 0).toString();
        String sqldeleteelem ="delete from `element` where id = '"+eve+"';";
        try {
            st.execute(sqldeleteelem);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void chercherelem(){
        try {
            String sql3 ="select * from `element` where nom like '%" + textField8.getText()+"%' OR id like '%" + textField8.getText()+"%';";
            st.execute(sql3);
            res = st.executeQuery(sql3);
            table2.setModel(DbUtils.resultSetToTableModel(res));
            textField8.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    //-----------------------------------------Professeurs----------------------------------------------
    public void ajouterprof(){
        try {
            String idelem = Objects.requireNonNull(comboBox2.getSelectedItem()).toString();
            idelem =idelem.substring(0, idelem.indexOf(' '));
            res =st.executeQuery("select max(id) from professeur;");
            System.out.println(res.next());
            String sqlelem="insert into `professeur` (id,nom,telephone,codeElem,Email) values('"+(Integer.parseInt(res.getString(1))+1)+"','"+textField9.getText()+"','"+textField10.getText()+"','"+idelem+"','"+textField11.getText()+"')";
            st.executeUpdate(sqlelem);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            textField9.setText("");
            textField10.setText("");
            textField11.setText("");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_elem_prof(){
        try {
            comboBox3.removeAllItems();
            comboBox3.setEditable(false);
            String sqlgetelem ="select id,nom from element;";
            res= null ;
            Statement dep =con.createStatement();
            res = dep.executeQuery(sqlgetelem);
            while(res.next()){
                comboBox3.addItem(res.getString(1)+ " " +res.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void modifierprof(){

        String idelem = Objects.requireNonNull(comboBox2.getSelectedItem()).toString();
        idelem =idelem.substring(0, idelem.indexOf(' '));

        try {
            int row = table4.getSelectedRow();
            DefaultTableModel model = (DefaultTableModel) table4.getModel();
            String eve3 =  model.getValueAt(row, 0).toString();
            Statement s=con.createStatement();
            String sql ="update `professeur` set nom = '"+textField9.getText()+"',telephone = '"+textField10.getText()+"',codeElem = '"+ idelem+"', Email = '"+textField11.getText()+"' where id = '"+eve3+"';";
            st.execute(sql);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void supprimerprof(){
        int row = table4.getSelectedRow();
        String eve = table4.getModel().getValueAt(row, 0).toString();
        String sqldeleteelem ="delete from `professeur` where id = '"+eve+"';";
        try {
            st.execute(sqldeleteelem);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void chercherprof(){
        try {
            String sql3 ="select * from `professeur` where nom like '%" + textField12.getText()+"%' OR id like '%" + textField12.getText()+"%' ;";
            st.execute(sql3);
            res = st.executeQuery(sql3);
            table4.setModel(DbUtils.resultSetToTableModel(res));
            textField12.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    //-----------------------------------------Etudiants----------------------------------------------
    public void ajouteretud(){
        try {
            String idoptetud = Objects.requireNonNull(comboBox4.getSelectedItem()).toString();
            idoptetud =  idoptetud.substring(0,  idoptetud.indexOf(' '));
            res =st.executeQuery("select max(id) from etudiant;");
            System.out.println(res.next());
            if (path == null){
                String imagedefault = "0x89504e470d0a1a0a0000000d494844520000009c000000a10806000000325cd63e000000097048597300000b1300000b1301009a9c18000005c869545874584d4c3a636f6d2e61646f62652e786d7000000000003c3f787061636b657420626567696e3d22efbbbf222069643d2257354d304d7043656869487a7265537a4e54637a6b633964223f3e203c783a786d706d65746120786d6c6e733a783d2261646f62653a6e733a6d6574612f2220783a786d70746b3d2241646f626520584d5020436f726520362e302d633030322037392e3136343438382c20323032302f30372f31302d32323a30363a35332020202020202020223e203c7264663a52444620786d6c6e733a7264663d22687474703a2f2f7777772e77332e6f72672f313939392f30322f32322d7264662d73796e7461782d6e7323223e203c7264663a4465736372697074696f6e207264663a61626f75743d222220786d6c6e733a786d703d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f2220786d6c6e733a64633d22687474703a2f2f7075726c2e6f72672f64632f656c656d656e74732f312e312f2220786d6c6e733a70686f746f73686f703d22687474703a2f2f6e732e61646f62652e636f6d2f70686f746f73686f702f312e302f2220786d6c6e733a786d704d4d3d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f6d6d2f2220786d6c6e733a73744576743d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f73547970652f5265736f757263654576656e74232220786d703a43726561746f72546f6f6c3d2241646f62652050686f746f73686f702032322e30202857696e646f7773292220786d703a437265617465446174653d22323032312d31322d30365431363a33313a31382b30313a30302220786d703a4d6f64696679446174653d22323032312d31322d30365431363a33343a32372b30313a30302220786d703a4d65746164617461446174653d22323032312d31322d30365431363a33343a32372b30313a3030222064633a666f726d61743d22696d6167652f706e67222070686f746f73686f703a436f6c6f724d6f64653d22332220786d704d4d3a496e7374616e636549443d22786d702e6969643a62633066643931632d393635652d633734362d396133352d3564633632363933616235622220786d704d4d3a446f63756d656e7449443d2261646f62653a646f6369643a70686f746f73686f703a38363032643864652d343231392d623534342d393835612d6132356431313339613939652220786d704d4d3a4f726967696e616c446f63756d656e7449443d22786d702e6469643a39353334356462352d353736642d643034632d613639332d336435623133636338373535223e203c786d704d4d3a486973746f72793e203c7264663a5365713e203c7264663a6c692073744576743a616374696f6e3d2263726561746564222073744576743a696e7374616e636549443d22786d702e6969643a39353334356462352d353736642d643034632d613639332d336435623133636338373535222073744576743a7768656e3d22323032312d31322d30365431363a33313a31382b30313a3030222073744576743a736f6674776172654167656e743d2241646f62652050686f746f73686f702032322e30202857696e646f777329222f3e203c7264663a6c692073744576743a616374696f6e3d227361766564222073744576743a696e7374616e636549443d22786d702e6969643a62633066643931632d393635652d633734362d396133352d356463363236393361623562222073744576743a7768656e3d22323032312d31322d30365431363a33343a32372b30313a3030222073744576743a736f6674776172654167656e743d2241646f62652050686f746f73686f702032322e30202857696e646f777329222073744576743a6368616e6765643d222f222f3e203c2f7264663a5365713e203c2f786d704d4d3a486973746f72793e203c2f7264663a4465736372697074696f6e3e203c2f7264663a5244463e203c2f783a786d706d6574613e203c3f787061636b657420656e643d2272223f3ec244d03b0000169849444154789ced9d795094f71d879f7df77ab9841040024a2d2823141435a6f140a3b61a63a51e3864a284d4ab4ed2b1d6369dc4c6496d9299766c6792297612f11813356023048d06945ac5e045e2944233564554bc388220d7b267ffa0cba0a02cb8bbefbecbfbccec3023ebbe9fdd7df85deffbfebeaaf6f6f6cbc0132828b89ebb1a200c08903a89c2a0402b0026a953280c1a4c82d4091406178a700a6e45114ec1ad68a40ee0c908828046a341101cffbbb4582c582c16ac56ab0b93c91745b86ea8542ab45a6d9760adadad54575773fefc792a2a2aa8a9a9e1debd7b188d466c361b1a8d061f1f1f020303898a8a62cc9831c4c7c713121282288a40a78066b3199bcd26e55bf31814e100ad568b5aadc666b371e5ca15bef9e61b727373292f2fa7baba9ae6e666875f2b24248461c3863169d22452525248484860d8b06100984c262c168babde862c50b5b7b77f07044b1d440af47a3d2a958adada5afefef7bf939f9fcf993367fa25585f848686f2dc73cfb170e142e6cf9f8fbfbf3f168b05936950ae46350c4ae1743a1d822070f9f265b66ddbc6eeddbbb979f3a6cb8f1b1717c78a152b484f4f272c2c0cb3d98cd96c76f9713d88c1259c5aad46abd5525d5dcd471f7d446666264d4d4d6ecf111515c5ba75ebc8c8c820383818a3d1385826198347385114311a8d6cdbb68d4d9b36515b5b2b7524468d1ac5a64d9b484b4b4310040c0683d4915c8df70ba752a9d0ebf5949595f1dbdffe9623478e481da9078b162d62f3e6cd444747d3d1d1e1cd33da06af5ef8d56834e8f57a76ecd8417272b247ca06909b9bcbe4c993d9b3670f7abd1eb55a2d752497e1b5c2e9743a6c361bebd7af67c58a154e9d79ba829a9a1a962d5bc6ef7ffffbaef5406fc42bbb549d4e474b4b0bafbcf20a79797952c7e9378b162de2e38f3fc6d7d7978e8e0ea9e33813efeb52edb22d59b24496b2416717bb64c9121a1b1bd1e97452c7712a5e259c56aba5adad8d254b9678ec78cd51befcf24b52535331180c5ed5bd7a8d706ab51ab55acdaa55ab642f9b9d63c78eb17af56a0441f09a89845708671f64bff7de7b6467674b1dc7a9ecd9b387b7de7a0bad568b4aa5923ace63e31593065114c9cbcb63f1e2c55ebb86959d9d4d5a5a9adc1787e5bff0abd3e9a8acac64faf4e9dcbe7d5bea382e232a2a8a53a74ef1d4534f61341aa58e3350e43d4b55a9540882c0ebafbfeed5b2015cbf7e9df5ebd7230882acbb56590ba7d7ebd9bb772f070e1c903a8a5bd8b76f1f393939e8f57aa9a30c18d976a91a8d868686069e7df659aaaaaaa48ee336468f1e4d696929a228caf1d226f976a91a8d862d5bb60c2ad9002e5cb8406666261a8d3c2fd696650b676fdd929292bc7eecd61b61616194969612191929b72b87e5d9c269341ab2b2b206a56c00b5b5b5ecdcb953968bc1b2134ea3d1505f5fcf8e1d3ba48e22299f7cf209f7eedd935dd72a4be10e1e3cc8952b57a48e2229959595e4e6e62ac2b912954a85cd6623272747ea281e41767636369b4d56eb72b2124eabd5525555c5e9d3a7a58ee211141717535e5e2eab4b9864259c2008ecdfbf9f7bf7ee491dc523686f6fe7e4c9934a0be74a4e9e3c2975048fe2c489135247e817b2114eabd572ebd62d4a4b4ba58ee251fcfbdfffa6b5b555364b24b2114ead56535656c69d3b77a48ee251dcbc7993dbb76f2bc2b982c1761acb115a5a5aa8a8a8e8d7966252228f94ffa7b2b252ea081e49595999d4111c4616c2d96761972e5d9238896722a761866c8433994cd4d4d4481dc523b97bf7aed4111c4636c2198d465a5a5aa48ee291d83f1739acc7c9423800b3d92ce76bf95d4a6b6bab6cb6fb9285702a95aa6baf5c859ec8697f3959080760b3d964f3a1ba1b8bc5229b93f8b210ce66b379d5dde7ce462e6b702013e1a0f34c83dcaefd7217f6adfee57013b82c84b3d74490f3ed71aec4cfcf4f36adbf2c8483cebf627f7f7fa963782401019dd5479516ce49d86c36b45a2d6161615247f148828282a48ee030b2110e60c48811d206f150424343a58ee030b210ce4e7474b4d4113c92d1a3474b1dc16164255c444484d4113c0ead564b424282d4311c4636c2d96c36e2e2e2baaaf42974121e1ecef0e1c36573164636c299cd6662626264d57db883888808860c19229bb330b211ce62b1e0e3e3437c7cbcd4513c8a091326a0d56a15e15cc5c48913a58ee0514c983041ea08fd4256c2d96c36e6cc9983afafafd4513c025114993061822c167cedc84a3893c9444c4c0cb1b1b15247f108264e9c48424282acb6ec92957056ab159d4ec7ac59b3a48ee2114c993205b55a2d9bf11bc84c383bb367cf963a82e4e8743a52535365d59d820c8533994c2427273376ec58a9a348ca9831631833668cacba5390a170f6e591850b174a1d4552162c5820abe5103bb2130e3ac7724b972eedba2c67b0e1ebeb4b4a4a8aec6403990a6732991839722473e6cc913a8a24bcf0c20b242626caae3b05990a671f28676464489cc4fda8d56ad6ac5903c8e382cb0791a570d0796bdcecd9b3494e4e963a8a5b494e4e66c68c19b2bd4757b6c2d9d7e47ef5ab5f491dc5adac59b306411064397e03190b079dad5c4a4a0ad3a74f973a8a5b983a752a0b162c906deb063217ce6ab5a256ab79fdf5d7a58ee272d46a351b376e44afd7cbb67503990b07d0d1d1c10b2fbc406a6aaad4515cca4f7ffa5366cf9e4d474787d4511e0b59d6da7a107b91de679e7986c6c646a9e3389d808000befaea2b121313e52e9c3c6b6d3d88d16864d4a851bcf5d65b52477109bffef5af1933668cdc6503bca48583ce318ecd6663e6cc99949494481dc7694c9d3a95a3478fa256abb1582c52c7795cbca38583ce73ac3a9d8ecccc4caf39e5e5e7e7c7962d5b1045d11b6403bc60d2d01d83c1405252121f7cf081d4519cc29ffef427c68c1983c160903a8ad3f02ae1a0733cf7b39ffd4cf6a7bd32323278edb5d764bde6d61b5e3386eb8e46a3a1a3a3839494148e1d3b26759c7e3365ca140a0b0b1145519627e81f81f78ce1ba63369bf1f3f3e3e38f3f66e4c89152c7e917a3468d62f7eeddf8fafa7a9b6c801776a9760c0603919191e4e5e511151525751c87888c8c243f3f9f11234678c512486f78ad70d0295d424202b9b9b91ebfc3d0d0a143f9ecb3cf888b8bf3aa49c28378b570d029dd840913f8fcf3cf193e7cb8d4717a2532329203070ef0ecb3cf7ab56c300884834ee9264f9e4c515111e3c78f973ace7d8c1d3b96a2a2229e79e619af970d068970d0295d6c6c2c5f7ef925f3e6cd933a0e00f3e7cfa7b0b090d1a3470f0ad9c00b8453a954e8f57a445144abd53ef2b9068381909010f2f3f379efbdf7fa7cbeabd0ebf5fce10f7f202f2f8fd0d0d03e65b3bf3f9d4e278b5a0c8f42b6c269341a445144afd7f3dffffe976ddbb671f3e6cd3ef78fb3576dd9b061035f7cf105494949ee09fc7f929292387cf8301b376ec46ab5f6b9b02b8a22e5e5e57cf4d14754555575c927d71202b25af81504019d4e077456d03b71e2045bb76ea5a4a4847bf7eef1831ffc80bcbc3c468d1ae55017258a22cdcdcdfcf9cf7fe6830f3ea0a9a9c965d903030379e38d37f8c52f7e81bfbfbfc3f9ce9e3dcba2458bb875eb164141414c99328555ab563179f2e4ae99b78c4a1f35c842389d4e87200898cd66cacaca3878f0209f7cf20957ae5ce9f1dc51a346919393c3b871e31cfa52351a0d1a8d86f2f272de7df75df2f3f39dba06268a22a9a9a9bcf9e69bc4c7c763369b1ddaad521445ce9c39c382050b7a2ddb191515c5d2a54b99376f1e4f3ffd74d795c01e7e2acc7385b38fcd00aaabab292a2a62dbb66d9c3b77aecf2fecc9279f64dfbe7dcc9c39938e8e0e876ea7b377c515151564656571f0e0c1c72a791e1313c3fcf9f359b16245d71ebc8efc01d8dff7a14387484f4fefb316aa4aa562dcb871a4a7a7b360c182ae9dde1d7ddf6ec6f384b3b73800252525ecdbb78fbd7bf7525f5fdfafd7f1f7f7e76f7ffb1be9e9e90eb72add256f6c6ca4a4a48463c78e71edda356edebc497d7d3dcdcdcd188d462c160b8220a0d7ebf1f7f727343494c8c848befffdef3373e64c264f9e4c606020e09868dddffbae5dbbf8f9cf7fdeef96f689279ee0a5975ee2c5175f64ead4a940e74de31e746993e70867ef36ebeaea282c2ce4d34f3fa5a0a0e0b1c7261b376ee4cd37dfc4c7c7a75f4b0f6ab5fabe59acc964a2a5a585d6d6d6ae31937d4ce9e7e787bfbf7f8fe7f7e78b164591f6f676de7efb6d366fdeecf0ffeb0d4110983b772e2fbffc3273e6cc213030108bc5e209e766a515ae7b8b525555c5a79f7e4a56561657af5e75ea71a64f9fce871f7ec8e8d1a307fc172f08022a95aaeba71d7b59cd8196d7b48bfd9ffffc87b56bd73afdea96d1a347f3eaabaf92969646585818369b4dcaf3b4d208d75db473e7ceb17bf76e727272a8adad75d931c3c2c2f8ddef7ec7f2e5cb1d9e25ba1a7babb673e74e366edc48434383cb8e151d1d4d7a7a3acb962debba824682cfc0bdc27517edf8f1e36cddba957dfbf6b9758c3169d224de7df75d66ce9c09b87f70ddfd33282a2a62d3a64d7cf5d5576e3b7e6060202b57ae64e5ca955d2508dc289e7b84b37fc856ab95a2a222b66eddcafefdfb5d79c8472208022fbdf4126bd7aeedda15ddd5e2755f433c75ea141f7ef821bb77ef966c26f9c4134fb06cd93256af5eddaf59f463e25ae11e6cd1de7fff7df2f3f35d71a801a1d168484d4d65cd9a354c9b36ad6b6ce6ac85d4ee9299cd668e1e3dca8e1d3bd8bf7fbfc72c59f8f8f8b07cf972d6ae5d4b6c6cacabc778ae134eafd7a352a9387bf62c999999ecd9b3c7633ee4def8d18f7ec4e2c58b494e4e263a3a1a1f1f9ffb7e6f369bbb2607bd2108428f32e9adadad5cbc7891c2c2420e1d3ae4d6aeb3bf040707b37cf97256ad5a456c6cacab66b5ce17ce3eebaaaaaae28f7ffc233b76ec904d1d28e85c9e49484860c68c19c4c6c6121111c1f0e1c3193a7428010101e8f5fa1ee731cd6633068381c6c646aaababb97af52a1515151c3a74888a8a0a4f5a07eb93c0c040d6af5fcfead5ab090f0f77f66933e70a278a221d1d1d646666f297bffc85dbb76f3be3652547a7d311121242707030818181f8fafa76b564168b85b6b6361a1b1ba9abab73e94cdb9dc4c4c4f0c61b6f9091918156ab75d6f8ce39c269b55ad46a35870f1fe69d77dee1cc9933ce08a7e001242727b361c3069e7ffe79679cab7dfcbbb64451a4a9a989b56bd7326fde3c45362fe3e4c993cc9d3b97952b5752575787288a8f754dde808553abd588a2c8e1c387494e4ee6af7ffdeb804328783edbb76f67ca9429ecddbbb7d771aca30c4838fb0cf4edb7df66fefcf97cfbedb7033ab882bca8acac64e9d2a5fcf297bfc466b3752d79f5877e8fe14451a4b6b696d5ab577bd49a9a827b993b772edbb66d232222a23f138afe4d1a4451e4faf5eb2c5ebc98afbffe7ac06115bc8389132772e8d02187eecbf83f8e4f1ab45a2d7575758a6c0a5d949696929a9acaddbb771d1ed339249c4aa542a552f1eaabaf2ab229dc477171311b366c40a3d138347b754838bd5e4f4e4e0e9f7df6d9630754f03eb66fdfcea953a71c9a44f4299c2008984c26afd9e44fc1f9984c26de79e71dac566b9fad5c9fc2e9743a4e9e3c496969a9d3022a781f478e1ca1b8b8b8cf56cea12e75d7ae5d72b9ef514122ac562b7bf7eeedf3798f144eabd572fbf66d8a8a8a9c164cc17b3972e408f7eedd7be48cf591c2a9d56afef18f7f70ebd62da78753f03eae5dbbc6912347062e1ca0b46e0afde28b2fbe78e4ef1f2a9c5aada6b9b99913274e383d9482f772ecd831eaeaea1edaca3d5438ad56cbe9d3a7b97efdbacbc229781fd5d5d5949494f45f3880c3870f2bb353857ef3a87b377a15cebed8ab5c4ca93010f2f2f2686a6aeab595eb5538ad564b595919e7cf9f77793805efe3ead5ab5cba74c971e1542a155f7ffdb5276c7ea22043ac56eb43bbd5878ee1bca904a482fb79d8a9d01ec269341aeaebebf9e73fffe9f2500ade4b7979396d6d6df7dd180e0f11eee6cd9bbd6ef3a9a0e028172f5ee4f2e5cb3dc671bd76a9d5d5d5b2ba5b5ec1f3e8e8e8a0b2b2b2c7e54abd0a77f9f265b78452f06e7adb9fb857e15cb9319ec2e0a1b79306bd0a67df095b41e171b06faadd9d1ec2d96c36c68f1fdf6376a1a0d01f020303193b766c8f56ae8770269389f8f8f8ae6dd7151406c2ac59b3888d8ded71f2a0877056ab159d4ec7dab56bdd164ec1fb58b1620540cf0d1cdbdbdbbf6b6f6fb7757f180c069bd56ab5a5a4a4d800e5a13cfaf578f1c5176d56abd56630186c0fb8f5dd43b77ad0e974545555f1dc73cf71e3c68d077fada0d02bf1f1f1141515111616d6dbb9f8876ff560341a898989212b2babc77eb70a0abd111e1e4e4e4e0e4f3df5d4432ffc78e40598068381e79f7f9e9d3b7776edc6ada0d01b212121646767939090f0c88d6dfabc89a6a3a383b4b434b2b3b3090909716a4805ef203e3e9e828202a64f9fdee72e4a7d0a67b3d930180c2c5cb890c2c2421213139d165441fecc9b378f828202264c98e0d0965d0e6fd7653018183f7e3c478f1ee5b5d75e7bac900af267c890216cdebc99dcdc5c860d1be6f0a684fdda72d56030101a1a4a6666269f7ffe39717171030aab206f7efce31f73fcf8717ef39bdfa052a9fa55b96640dbe6db4b1a353434b07dfb76de7fff7de5eefc41c0d34f3fcdba75eb484d4d45afd70fa476c3e3d569b05730be76ed1a5bb76e65cb962d3435350de4a5143c98989818d6ad5bc72bafbc82bfbfffe354a7714e61107b35e74b972e919d9dcdae5dbba8acac7c9c9754f0007ef8c31ff2f2cb2fb364c9124243439d51cedcb9a58fecdbe9d7d5d571e0c001b2b2b2387bf6ac335e5ac14da8d56ae6ce9d4b4646063ff9c94f104511b3d9ecac2bc05d534dd0ded51a0c06bef9e61b76edda45414101d5d5d5ce3c8c8213193972242929292c5dba94a4a4240441707661377075bdd4eef542ebebeb292929a1a0a080fcfc7caf29fc2667222323494d4d65f6ecd94c9932a5eb824917162b765f09727bab0770e7ce1dfef5af7f919b9b4b717131972e5d52f6307113b1b1b14c9d3a95b4b434c68e1dcbd0a143019c313e7304f7d6bcb763af3e08d0d2d2c2850b17387ffe3cc5c5c51c3f7e9c5bb76e7974315f39f1e4934f326dda3466cd9ac5b871e3484c4c24202000c0996333479146b8eed80bfadaa9adade5ead5ab9c3b778ea2a2222e5ebcc8f5ebd7696d6d952aa26c100481e1c387131d1dcd8c193398366d1a3131310c1b36aceb396e6ac91e86f4c23d48f7ae173acb78dfb871836fbffd96ab57af525e5ecee9d3a7b971e3062d2d2d122695168d46434848088989894c9a3489989818e2e2e28889892128280841e83c8964b158309bcd9ed263789e70ddb157c0d16ab5f7dd50dbdcdc4c4d4d0d75757554565672f6ec592e5cb8404d4d0df5f5f5343434f4eb748b273364c810828383090e0e66c488118c1f3f9e71e3c61111114178783843870ebdefb3319bcd582c164f11ec413c5bb887a156ab51abd55d7fc576dadada686969a1b6b696eaea6aeaebeba9adade5ce9d3bdcb87183ebd7af73ebd62deedebd8bd168c468344ad6bd68341a743a1d7abd9e909010c2c3c3f9def7bec7881123080b0bbbefdf828282f0f3f3eb5103c1c3e5ea0d790ad71bf6d6501004d46a75af15514c2613cdcdcdb4b4b4d0d6d6466b6b2b6d6d6d343737d3d8d8c8ddbb77f9eebbef686c6ca4b9b999d6d6565a5a5a30180c188dc6877ec182202008025aad169d4e87288a040404e0e7e74740400041414104070777fd0c0808c0c7c7073f3fbfaee7f8f9f9f5ba9f9ad56aed3aa6fd2163bc47b8bee82e64f79f8ed2fd0b7ff04bb7bfb6fdd19fd7b45aad5d0f99cbe4080d03ab232d43eca2f477bd6fa0f5dd07813c0362d008375014719ccb806ade2b280c14453805b7a208a7e05614e114dc8a229c825b11006d9fcf5250700e5a0d500b28154014dcc1ddff01c192242a7f85bd400000000049454e44ae426082";
                PreparedStatement sqletud1 =con.prepareStatement("insert into `etudiant` (id,img,nom,telephone,Email,options) values('"+(Integer.parseInt(res.getString(1))+1)+"',"+imagedefault+",'"+textField13.getText()+"','"+textField14.getText()+"','"+textField15.getText()+"','"+idoptetud+"')");
                sqletud1.executeUpdate();
            }else {
                InputStream img= new FileInputStream(new File(path));
                PreparedStatement sqletud=con.prepareStatement("insert into `etudiant` (id,img,nom,telephone,Email,options) values('"+(Integer.parseInt(res.getString(1))+1)+"',?,'"+textField13.getText()+"','"+textField14.getText()+"','"+textField15.getText()+"','"+idoptetud+"')");
                sqletud.setBlob(1, img);
                sqletud.executeUpdate();
            }
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            textField13.setText("");
            textField14.setText("");
            textField15.setText("");
            path = null;
        }catch (FileNotFoundException | SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_opt_etud(){
        try {
            comboBox4.removeAllItems();
            comboBox4.setEditable(false);
            String sqletudopt ="select id,nom from options;";
            res= null ;
            Statement dep =con.createStatement();
            res = dep.executeQuery(sqletudopt);
            while(res.next()){
                comboBox4.addItem(res.getString(1)+ " " + res.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void modifieretud(){
        String idoptetud = Objects.requireNonNull(comboBox4.getSelectedItem()).toString();
        idoptetud =  idoptetud.substring(0,  idoptetud.indexOf(' '));
        try {
            int row = table5.getSelectedRow();
            String eve = table5.getModel().getValueAt(row, 0).toString();
            if (path==null){
                String imagedefault = "0x89504e470d0a1a0a0000000d494844520000009c000000a10806000000325cd63e000000097048597300000b1300000b1301009a9c18000005c869545874584d4c3a636f6d2e61646f62652e786d7000000000003c3f787061636b657420626567696e3d22efbbbf222069643d2257354d304d7043656869487a7265537a4e54637a6b633964223f3e203c783a786d706d65746120786d6c6e733a783d2261646f62653a6e733a6d6574612f2220783a786d70746b3d2241646f626520584d5020436f726520362e302d633030322037392e3136343438382c20323032302f30372f31302d32323a30363a35332020202020202020223e203c7264663a52444620786d6c6e733a7264663d22687474703a2f2f7777772e77332e6f72672f313939392f30322f32322d7264662d73796e7461782d6e7323223e203c7264663a4465736372697074696f6e207264663a61626f75743d222220786d6c6e733a786d703d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f2220786d6c6e733a64633d22687474703a2f2f7075726c2e6f72672f64632f656c656d656e74732f312e312f2220786d6c6e733a70686f746f73686f703d22687474703a2f2f6e732e61646f62652e636f6d2f70686f746f73686f702f312e302f2220786d6c6e733a786d704d4d3d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f6d6d2f2220786d6c6e733a73744576743d22687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f73547970652f5265736f757263654576656e74232220786d703a43726561746f72546f6f6c3d2241646f62652050686f746f73686f702032322e30202857696e646f7773292220786d703a437265617465446174653d22323032312d31322d30365431363a33313a31382b30313a30302220786d703a4d6f64696679446174653d22323032312d31322d30365431363a33343a32372b30313a30302220786d703a4d65746164617461446174653d22323032312d31322d30365431363a33343a32372b30313a3030222064633a666f726d61743d22696d6167652f706e67222070686f746f73686f703a436f6c6f724d6f64653d22332220786d704d4d3a496e7374616e636549443d22786d702e6969643a62633066643931632d393635652d633734362d396133352d3564633632363933616235622220786d704d4d3a446f63756d656e7449443d2261646f62653a646f6369643a70686f746f73686f703a38363032643864652d343231392d623534342d393835612d6132356431313339613939652220786d704d4d3a4f726967696e616c446f63756d656e7449443d22786d702e6469643a39353334356462352d353736642d643034632d613639332d336435623133636338373535223e203c786d704d4d3a486973746f72793e203c7264663a5365713e203c7264663a6c692073744576743a616374696f6e3d2263726561746564222073744576743a696e7374616e636549443d22786d702e6969643a39353334356462352d353736642d643034632d613639332d336435623133636338373535222073744576743a7768656e3d22323032312d31322d30365431363a33313a31382b30313a3030222073744576743a736f6674776172654167656e743d2241646f62652050686f746f73686f702032322e30202857696e646f777329222f3e203c7264663a6c692073744576743a616374696f6e3d227361766564222073744576743a696e7374616e636549443d22786d702e6969643a62633066643931632d393635652d633734362d396133352d356463363236393361623562222073744576743a7768656e3d22323032312d31322d30365431363a33343a32372b30313a3030222073744576743a736f6674776172654167656e743d2241646f62652050686f746f73686f702032322e30202857696e646f777329222073744576743a6368616e6765643d222f222f3e203c2f7264663a5365713e203c2f786d704d4d3a486973746f72793e203c2f7264663a4465736372697074696f6e3e203c2f7264663a5244463e203c2f783a786d706d6574613e203c3f787061636b657420656e643d2272223f3ec244d03b0000169849444154789ced9d795094f71d879f7df77ab9841040024a2d2823141435a6f140a3b61a63a51e3864a284d4ab4ed2b1d6369dc4c6496d9299766c6792297612f11813356023048d06945ac5e045e2944233564554bc388220d7b267ffa0cba0a02cb8bbefbecbfbccec3023ebbe9fdd7df85deffbfebeaaf6f6f6cbc0132828b89ebb1a200c08903a89c2a0402b0026a953280c1a4c82d4091406178a700a6e45114ec1ad68a40ee0c908828046a341101cffbbb4582c582c16ac56ab0b93c91745b86ea8542ab45a6d9760adadad54575773fefc792a2a2aa8a9a9e1debd7b188d466c361b1a8d061f1f1f020303898a8a62cc9831c4c7c713121282288a40a78066b3199bcd26e55bf31814e100ad568b5aadc666b371e5ca15bef9e61b727373292f2fa7baba9ae6e666875f2b24248461c3863169d22452525248484860d8b06100984c262c168babde862c50b5b7b77f07044b1d440af47a3d2a958adada5afefef7bf939f9fcf993367fa25585f848686f2dc73cfb170e142e6cf9f8fbfbf3f168b05936950ae46350c4ae1743a1d822070f9f265b66ddbc6eeddbbb979f3a6cb8f1b1717c78a152b484f4f272c2c0cb3d98cd96c76f9713d88c1259c5aad46abd5525d5dcd471f7d446666264d4d4d6ecf111515c5ba75ebc8c8c820383818a3d1385826198347385114311a8d6cdbb68d4d9b36515b5b2b7524468d1ac5a64d9b484b4b4310040c0683d4915c8df70ba752a9d0ebf5949595f1dbdffe9623478e481da9078b162d62f3e6cd444747d3d1d1e1cd33da06af5ef8d56834e8f57a76ecd8417272b247ca06909b9bcbe4c993d9b3670f7abd1eb55a2d752497e1b5c2e9743a6c361bebd7af67c58a154e9d79ba829a9a1a962d5bc6ef7ffffbaef5406fc42bbb549d4e474b4b0bafbcf20a79797952c7e9378b162de2e38f3fc6d7d7978e8e0ea9e33813efeb52edb22d59b24496b2416717bb64c9121a1b1bd1e97452c7712a5e259c56aba5adad8d254b9678ec78cd51befcf24b52535331180c5ed5bd7a8d706ab51ab55acdaa55ab642f9b9d63c78eb17af56a0441f09a89845708671f64bff7de7b6467674b1dc7a9ecd9b387b7de7a0bad568b4aa5923ace63e31593065114c9cbcb63f1e2c55ebb86959d9d4d5a5a9adc1787e5bff0abd3e9a8acac64faf4e9dcbe7d5bea382e232a2a8a53a74ef1d4534f61341aa58e3350e43d4b55a9540882c0ebafbfeed5b2015cbf7e9df5ebd7230882acbb56590ba7d7ebd9bb772f070e1c903a8a5bd8b76f1f393939e8f57aa9a30c18d976a91a8d868686069e7df659aaaaaaa48ee336468f1e4d696929a228caf1d226f976a91a8d862d5bb60c2ad9002e5cb8406666261a8d3c2fd696650b676fdd929292bc7eecd61b61616194969612191929b72b87e5d9c269341ab2b2b206a56c00b5b5b5ecdcb953968bc1b2134ea3d1505f5fcf8e1d3ba48e22299f7cf209f7eedd935dd72a4be10e1e3cc8952b57a48e2229959595e4e6e62ac2b912954a85cd6623272747ea281e41767636369b4d56eb72b2124eabd5525555c5e9d3a7a58ee211141717535e5e2eab4b9864259c2008ecdfbf9f7bf7ee491dc523686f6fe7e4c9934a0be74a4e9e3c2975048fe2c489135247e817b2114eabd572ebd62d4a4b4ba58ee251fcfbdfffa6b5b555364b24b2114ead56535656c69d3b77a48ee251dcbc7993dbb76f2bc2b982c1761acb115a5a5aa8a8a8e8d7966252228f94ffa7b2b252ea081e49595999d4111c4616c2d96761972e5d9238896722a761866c8433994cd4d4d4481dc523b97bf7aed4111c4636c2198d465a5a5aa48ee291d83f1739acc7c9423800b3d92ce76bf95d4a6b6bab6cb6fb9285702a95aa6baf5c859ec8697f3959080760b3d964f3a1ba1b8bc5229b93f8b210ce66b379d5dde7ce462e6b702013e1a0f34c83dcaefd7217f6adfee57013b82c84b3d74490f3ed71aec4cfcf4f36adbf2c8483cebf627f7f7fa963782401019dd5479516ce49d86c36b45a2d6161615247f148828282a48ee030b2110e60c48811d206f150424343a58ee030b210ce4e7474b4d4113c92d1a3474b1dc16164255c444484d4113c0ead564b424282d4311c4636c2d96c36e2e2e2baaaf42974121e1ecef0e1c36573164636c299cd6662626264d57db883888808860c19229bb330b211ce62b1e0e3e3437c7cbcd4513c8a091326a0d56a15e15cc5c48913a58ee0514c983041ea08fd4256c2d96c36e6cc9983afafafd4513c025114993061822c167cedc84a3893c9444c4c0cb1b1b15247f108264e9c48424282acb6ec92957056ab159d4ec7ac59b3a48ee2114c993205b55a2d9bf11bc84c383bb367cf963a82e4e8743a52535365d59d820c8533994c2427273376ec58a9a348ca9831631833668cacba5390a170f6e591850b174a1d4552162c5820abe5103bb2130e3ac7724b972eedba2c67b0e1ebeb4b4a4a8aec6403990a6732991839722473e6cc913a8a24bcf0c20b242626caae3b05990a671f28676464489cc4fda8d56ad6ac5903c8e382cb0791a570d0796bdcecd9b3494e4e963a8a5b494e4e66c68c19b2bd4757b6c2d9d7e47ef5ab5f491dc5adac59b306411064397e03190b079dad5c4a4a0ad3a74f973a8a5b983a752a0b162c906deb063217ce6ab5a256ab79fdf5d7a58ee272d46a351b376e44afd7cbb67503990b07d0d1d1c10b2fbc406a6aaad4515cca4f7ffa5366cf9e4d474787d4511e0b59d6da7a107b91de679e7986c6c646a9e3389d808000befaea2b121313e52e9c3c6b6d3d88d16864d4a851bcf5d65b52477109bffef5af1933668cdc6503bca48583ce318ecd6663e6cc99949494481dc7694c9d3a95a3478fa256abb1582c52c7795cbca38583ce73ac3a9d8ecccc4caf39e5e5e7e7c7962d5b1045d11b6403bc60d2d01d83c1405252121f7cf081d4519cc29ffef427c68c1983c160903a8ad3f02ae1a0733cf7b39ffd4cf6a7bd32323278edb5d764bde6d61b5e3386eb8e46a3a1a3a3839494148e1d3b26759c7e3365ca140a0b0b1145519627e81f81f78ce1ba63369bf1f3f3e3e38f3f66e4c89152c7e917a3468d62f7eeddf8fafa7a9b6c801776a9760c0603919191e4e5e511151525751c87888c8c243f3f9f11234678c512486f78ad70d0295d424202b9b9b91ebfc3d0d0a143f9ecb3cf888b8bf3aa49c28378b570d029dd840913f8fcf3cf193e7cb8d4717a2532329203070ef0ecb3cf7ab56c300884834ee9264f9e4c515111e3c78f973ace7d8c1d3b96a2a2229e79e619af970d068970d0295d6c6c2c5f7ef925f3e6cd933a0e00f3e7cfa7b0b090d1a3470f0ad9c00b8453a954e8f57a445144abd53ef2b9068381909010f2f3f379efbdf7fa7cbeabd0ebf5fce10f7f202f2f8fd0d0d03e65b3bf3f9d4e278b5a0c8f42b6c269341a445144afd7f3dffffe976ddbb671f3e6cd3ef78fb3576dd9b061035f7cf105494949ee09fc7f929292387cf8301b376ec46ab5f6b9b02b8a22e5e5e57cf4d14754555575c927d71202b25af81504019d4e077456d03b71e2045bb76ea5a4a4847bf7eef1831ffc80bcbc3c468d1ae55017258a22cdcdcdfcf9cf7fe6830f3ea0a9a9c965d903030379e38d37f8c52f7e81bfbfbfc3f9ce9e3dcba2458bb875eb164141414c99328555ab563179f2e4ae99b78c4a1f35c842389d4e87200898cd66cacaca3878f0209f7cf20957ae5ce9f1dc51a346919393c3b871e31cfa52351a0d1a8d86f2f272de7df75df2f3f39dba06268a22a9a9a9bcf9e69bc4c7c763369b1ddaad521445ce9c39c382050b7a2ddb191515c5d2a54b99376f1e4f3ffd74d795c01e7e2acc7385b38fcd00aaabab292a2a62dbb66d9c3b77aecf2fecc9279f64dfbe7dcc9c39938e8e0e876ea7b377c515151564656571f0e0c1c72a791e1313c3fcf9f359b16245d71ebc8efc01d8dff7a14387484f4fefb316aa4aa562dcb871a4a7a7b360c182ae9dde1d7ddf6ec6f384b3b73800252525ecdbb78fbd7bf7525f5fdfafd7f1f7f7e76f7ffb1be9e9e90eb72add256f6c6ca4a4a48463c78e71edda356edebc497d7d3dcdcdcd188d462c160b8220a0d7ebf1f7f727343494c8c848befffdef3373e64c264f9e4c606020e09868dddffbae5dbbf8f9cf7fdeef96f689279ee0a5975ee2c5175f64ead4a940e74de31e746993e70867ef36ebeaea282c2ce4d34f3fa5a0a0e0b1c7261b376ee4cd37dfc4c7c7a75f4b0f6ab5fabe59acc964a2a5a585d6d6d6ae31937d4ce9e7e787bfbf7f8fe7f7e78b164591f6f676de7efb6d366fdeecf0ffeb0d4110983b772e2fbffc3273e6cc213030108bc5e209e766a515ae7b8b525555c5a79f7e4a56561657af5e75ea71a64f9fce871f7ec8e8d1a307fc172f08022a95aaeba71d7b59cd8196d7b48bfd9ffffc87b56bd73afdea96d1a347f3eaabaf92969646585818369b4dcaf3b4d208d75db473e7ceb17bf76e727272a8adad75d931c3c2c2f8ddef7ec7f2e5cb1d9e25ba1a7babb673e74e366edc48434383cb8e151d1d4d7a7a3acb962debba824682cfc0bdc27517edf8f1e36cddba957dfbf6b9758c3169d224de7df75d66ce9c09b87f70ddfd33282a2a62d3a64d7cf5d5576e3b7e6060202b57ae64e5ca955d2508dc289e7b84b37fc856ab95a2a222b66eddcafefdfb5d79c8472208022fbdf4126bd7aeedda15ddd5e2755f433c75ea141f7ef821bb77ef966c26f9c4134fb06cd93256af5eddaf59f463e25ae11e6cd1de7fff7df2f3f35d71a801a1d168484d4d65cd9a354c9b36ad6b6ce6ac85d4ee9299cd668e1e3dca8e1d3bd8bf7fbfc72c59f8f8f8b07cf972d6ae5d4b6c6cacabc778ae134eafd7a352a9387bf62c999999ecd9b3c7633ee4def8d18f7ec4e2c58b494e4e263a3a1a1f1f9ffb7e6f369bbb2607bd2108428f32e9adadad5cbc7891c2c2420e1d3ae4d6aeb3bf040707b37cf97256ad5a456c6cacab66b5ce17ce3eebaaaaaae28f7ffc233b76ec904d1d28e85c9e49484860c68c19c4c6c6121111c1f0e1c3193a7428010101e8f5fa1ee731cd6633068381c6c646aaababb97af52a1515151c3a74888a8a0a4f5a07eb93c0c040d6af5fcfead5ab090f0f77f66933e70a278a221d1d1d646666f297bffc85dbb76f3be3652547a7d311121242707030818181f8fafa76b564168b85b6b6361a1b1ba9abab73e94cdb9dc4c4c4f0c61b6f9091918156ab75d6f8ce39c269b55ad46a35870f1fe69d77dee1cc9933ce08a7e001242727b361c3069e7ffe79679cab7dfcbbb64451a4a9a989b56bd7326fde3c45362fe3e4c993cc9d3b97952b5752575787288a8f754dde808553abd588a2c8e1c387494e4ee6af7ffdeb804328783edbb76f67ca9429ecddbbb7d771aca30c4838fb0cf4edb7df66fefcf97cfbedb7033ab882bca8acac64e9d2a5fcf297bfc466b3752d79f5877e8fe14451a4b6b696d5ab577bd49a9a827b993b772edbb66d232222a23f138afe4d1a4451e4faf5eb2c5ebc98afbffe7ac06115bc8389132772e8d02187eecbf83f8e4f1ab45a2d7575758a6c0a5d949696929a9acaddbb771d1ed339249c4aa542a552f1eaabaf2ab229dc477171311b366c40a3d138347b754838bd5e4f4e4e0e9f7df6d9630754f03eb66fdfcea953a71c9a44f4299c2008984c26afd9e44fc1f9984c26de79e71dac566b9fad5c9fc2e9743a4e9e3c496969a9d3022a781f478e1ca1b8b8b8cf56cea12e75d7ae5d72b9ef514122ac562b7bf7eeedf3798f144eabd572fbf66d8a8a8a9c164cc17b3972e408f7eedd7be48cf591c2a9d56afef18f7f70ebd62da78753f03eae5dbbc6912347062e1ca0b46e0afde28b2fbe78e4ef1f2a9c5aada6b9b99913274e383d9482f772ecd831eaeaea1edaca3d5438ad56cbe9d3a7b97efdbacbc229781fd5d5d5949494f45f3880c3870f2bb353857ef3a87b377a15cebed8ab5c4ca93010f2f2f2686a6aeab595eb5538ad564b595919e7cf9f77793805efe3ead5ab5cba74c971e1542a155f7ffdb5276c7ea22043ac56eb43bbd5878ee1bca904a482fb79d8a9d01ec269341aeaebebf9e73fffe9f2500ade4b7979396d6d6df7dd180e0f11eee6cd9bbd6ef3a9a0e028172f5ee4f2e5cb3dc671bd76a9d5d5d5b2ba5b5ec1f3e8e8e8a0b2b2b2c7e54abd0a77f9f265b78452f06e7adb9fb857e15cb9319ec2e0a1b79306bd0a67df095b41e171b06faadd9d1ec2d96c36c68f1fdf6376a1a0d01f020303193b766c8f56ae8770269389f8f8f8ae6dd7151406c2ac59b3888d8ded71f2a0877056ab159d4ec7dab56bdd164ec1fb58b1620540cf0d1cdbdbdbbf6b6f6fb7757f180c069bd56ab5a5a4a4d800e5a13cfaf578f1c5176d56abd56630186c0fb8f5dd43b77ad0e974545555f1dc73cf71e3c68d077fada0d02bf1f1f1141515111616d6dbb9f8876ff560341a898989212b2babc77eb70a0abd111e1e4e4e4e0e4f3df5d4432ffc78e40598068381e79f7f9e9d3b7776edc6ada0d01b212121646767939090f0c88d6dfabc89a6a3a383b4b434b2b3b3090909716a4805ef203e3e9e828202a64f9fdee72e4a7d0a67b3d930180c2c5cb890c2c2421213139d165441fecc9b378f828202264c98e0d0965d0e6fd7653018183f7e3c478f1ee5b5d75e7bac900af267c890216cdebc99dcdc5c860d1be6f0a684fdda72d56030101a1a4a6666269f7ffe39717171030aab206f7efce31f73fcf8717ef39bdfa052a9fa55b96640dbe6db4b1a353434b07dfb76de7fff7de5eefc41c0d34f3fcdba75eb484d4d45afd70fa476c3e3d569b05730be76ed1a5bb76e65cb962d3435350de4a5143c98989818d6ad5bc72bafbc82bfbfffe354a7714e61107b35e74b972e919d9dcdae5dbba8acac7c9c9754f0007ef8c31ff2f2cb2fb364c9124243439d51cedcb9a58fecdbe9d7d5d571e0c001b2b2b2387bf6ac335e5ac14da8d56ae6ce9d4b4646063ff9c94f104511b3d9ecac2bc05d534dd0ded51a0c06bef9e61b76edda45414101d5d5d5ce3c8c8213193972242929292c5dba94a4a4240441707661377075bdd4eef542ebebeb292929a1a0a080fcfc7caf29fc2667222323494d4d65f6ecd94c9932a5eb824917162b765f09727bab0770e7ce1dfef5af7f919b9b4b717131972e5d52f6307113b1b1b14c9d3a95b4b434c68e1dcbd0a143019c313e7304f7d6bcb763af3e08d0d2d2c2850b17387ffe3cc5c5c51c3f7e9c5bb76e7974315f39f1e4934f326dda3466cd9ac5b871e3484c4c24202000c0996333479146b8eed80bfadaa9adade5ead5ab9c3b778ea2a2222e5ebcc8f5ebd7696d6d952aa26c100481e1c387131d1dcd8c193398366d1a3131310c1b36aceb396e6ac91e86f4c23d48f7ae173acb78dfb871836fbffd96ab57af525e5ecee9d3a7b971e3062d2d2d122695168d46434848088989894c9a3489989818e2e2e28889892128280841e83c8964b158309bcd9ed263789e70ddb157c0d16ab5f7dd50dbdcdc4c4d4d0d75757554565672f6ec592e5cb8404d4d0df5f5f5343434f4eb748b273364c810828383090e0e66c488118c1f3f9e71e3c61111114178783843870ebdefb3319bcd582c164f11ec413c5bb887a156ab51abd55d7fc576dadada686969a1b6b696eaea6aeaebeba9adade5ce9d3bdcb87183ebd7af73ebd62deedebd8bd168c468344ad6bd68341a743a1d7abd9e909010c2c3c3f9def7bec7881123080b0bbbefdf828282f0f3f3eb5103c1c3e5ea0d790ad71bf6d6501004d46a75af15514c2613cdcdcdb4b4b4d0d6d6466b6b2b6d6d6d343737d3d8d8c8ddbb77f9eebbef686c6ca4b9b999d6d6565a5a5a30180c188dc6877ec182202008025aad169d4e87288a040404e0e7e74740400041414104070777fd0c0808c0c7c7073f3fbfaee7f8f9f9f5ba9f9ad56aed3aa6fd2163bc47b8bee82e64f79f8ed2fd0b7ff04bb7bfb6fdd19fd7b45aad5d0f99cbe4080d03ab232d43eca2f477bd6fa0f5dd07813c0362d008375014719ccb806ade2b280c14453805b7a208a7e05614e114dc8a229c825b11006d9fcf5250700e5a0d500b28154014dcc1ddff01c192242a7f85bd400000000049454e44ae426082";
                PreparedStatement sqlmodetud1=con.prepareStatement("update `etudiant` set nom = '"+textField13.getText()+"',telephone = '"+textField14.getText()+"', img = "+imagedefault+", options = '"+ idoptetud +"', Email = '"+textField15.getText()+"' where id = '"+eve+"';");
                sqlmodetud1.executeUpdate();
            }else {
                InputStream img= new FileInputStream(new File(path));
                PreparedStatement sqlmodetud2=con.prepareStatement("update `etudiant` set nom = '"+textField13.getText()+"',telephone = '"+textField14.getText()+"', img = ?, options = '"+ idoptetud +"', Email = '"+textField15.getText()+"' where id = '"+eve+"';");
                sqlmodetud2.setBlob(1, img);
                sqlmodetud2.executeUpdate();
            }

            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (FileNotFoundException | SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
        path = null;
    }
    public void supprimeretud(){
        int row = table5.getSelectedRow();
        String eve = table5.getModel().getValueAt(row, 0).toString();
        String sqldeletetud ="delete from `etudiant` where id = '"+eve+"';";
        try {
            st.execute(sqldeletetud);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void chercheretud(){
        try {
            String sql3 ="select * from `etudiant` where nom like '%" + textField16.getText()+"%';";
            st.execute(sql3);
            res = st.executeQuery(sql3);
            table5.setModel(DbUtils.resultSetToTableModel(res));
            textField16.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    //-----------------------------------------Notes----------------------------------------------
    public void ajouternote(){
        try {
            String idelemnote = Objects.requireNonNull(comboBox5.getSelectedItem()).toString();
            String idetudnote = Objects.requireNonNull(comboBox6.getSelectedItem()).toString();
            idelemnote = idelemnote.substring(0,idelemnote.indexOf(' '));
            idetudnote  = idetudnote .substring(0,idetudnote.indexOf(' '));
            System.out.println(res.next());
            String sqlnote="insert into `evaluer` (codeElem,codeEtud,note,dateEva) values('"+idetudnote+"','"+idelemnote+"','"+textField20.getText()+"','"+annee.getText()+"/"+mois.getText()+"/"+jour.getText()+"')";
            st.executeUpdate(sqlnote);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement ajouter!");
            textField20.setText("");
            jour.setText("");
            mois.setText("");
            annee.setText("");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_etud_note(){
        try {
            String valeur;
            comboBox5.removeAllItems();
            comboBox5.setEditable(false);
            String sqlnotegetelem ="select id,nom from etudiant;";
            ResultSet rs_etud_note;
            Statement dep =con.createStatement();
            rs_etud_note = dep.executeQuery(sqlnotegetelem);
            while(rs_etud_note.next()){
                comboBox5.addItem(rs_etud_note.getString(1) + " " + rs_etud_note.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void get_elem_note(){
        try {
            comboBox6.removeAllItems();
            comboBox6.setEditable(false);
            String sqlnotegetetud ="select element.id, element.nom, etudiant.id, options.id, etudiant.options, element.options  from etudiant, element, options where element.options = etudiant.options AND element.options = options.id AND etudiant.options = options.id AND etudiant.id= '"+selectedValue+"' Group by element.id";
            ResultSet rs_elem_note;
            Statement dep =con.createStatement();
            rs_elem_note = dep.executeQuery(sqlnotegetetud);
            while( rs_elem_note.next()){
                comboBox6.addItem(rs_elem_note.getString(1) + " " + rs_elem_note.getString(2));
            }

        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }

    }
    public void modifiernote(){
        String idelemnote = Objects.requireNonNull(comboBox6.getSelectedItem()).toString();
        String idetudnote = Objects.requireNonNull(comboBox5.getSelectedItem()).toString();
        idelemnote = idelemnote.substring(0,idelemnote.indexOf(' '));
        idetudnote  = idetudnote .substring(0,idetudnote.indexOf(' '));
        int row = table6.getSelectedRow();
        // DefaultTableModel model = (DefaultTableModel) table6.getModel();
        String eve = table6.getModel().getValueAt(row, 0).toString();
        String eve2 = table6.getModel().getValueAt(row, 2).toString();
        String eve3 = table6.getModel().getValueAt(row, 4).toString();
        String eve4 = table6.getModel().getValueAt(row, 5).toString();
        String sql ="update `evaluer` set codeElem = '"+ idelemnote +"',codeEtud = '"+idetudnote+"',note = '"+textField20.getText()+"', dateEva = '"+annee.getText()+"/"+mois.getText()+"/"+jour.getText()+"' where codeElem = '"+eve+"' AND codeEtud = '"+eve2+"' AND note = '"+eve3+"' AND dateEva = '"+eve4+"';";
        try {
            String sqll ="set sql_mode='';";
            st.execute(sqll);
            st.execute(sql);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement modifier!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
        table_load();
    }
    public void supprimernote(){
        int row = table6.getSelectedRow();
        String eve = table6.getModel().getValueAt(row, 0).toString();
        String eve2 = table6.getModel().getValueAt(row, 2).toString();
        String eve3 = table6.getModel().getValueAt(row, 4).toString();
        String eve4 = table6.getModel().getValueAt(row, 5).toString();
        String sqldeletetud ="delete from `evaluer` where codeElem = '"+eve+"' AND codeEtud = '"+eve2+"' AND note = '"+eve3+"' AND dateEva = '"+eve4+"';";
        try {
            st.execute(sqldeletetud);
            table_load();
            JOptionPane.showMessageDialog(null, "Enregistrement Supprimer!");
        }catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "ERROR: "+e1.getMessage());
            e1.printStackTrace();
        }
    }
    public void cherchernote(){
        try {
            String sql3 ="select evaluer.dateEva,evaluer.note,evaluer.codeEtud,etudiant.nom,evaluer.codeElem from `evaluer`, etudiant where evaluer.codeEtud=etudiant.id and (codeEtud like'" + textField21.getText()+"' or etudiant.nom like '%" + textField21.getText()+"%') group by evaluer.dateEva,evaluer.note,evaluer.codeEtud,etudiant.nom,evaluer.codeElem  ;";
            st.execute(sql3);
            res = st.executeQuery(sql3);
            table6.setModel(DbUtils.resultSetToTableModel(res));
            textField21.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERROR: "+e.getMessage());
        }
    }

    private void createUIComponents() {
        URL acturl = ClassLoader.getSystemResource("Reload_Icon_Blue.svg.png");
        URL decurl = ClassLoader.getSystemResource("OOjs_UI_icon_logOut-ltr-progressive.svg.png");
        act = new JLabel(new ImageIcon(acturl));
        deco = new JLabel(new ImageIcon(decurl));
        imglabel = new JLabel(new ImageIcon(path));
    }
}
