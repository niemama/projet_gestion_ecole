package main;

import com.formdev.flatlaf.FlatDarculaLaf;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.*;

public class PageConnexion extends JFrame {
    //nom d'utlisateur
    private JTextField nom;
    //mot de passe
    private JPasswordField mdp;
    //champs static pour les getter du nom d'utilisateur et du mot de passe
    private static String nomstring;
    private static String mdpstring;
    //boutton de connexion
    private JButton connexion;
    //panel de connexion
    private JPanel mainPanel;
    //logo de java et mysql sur la page de connexion
  private JLabel label1;//logo java
    private JLabel label2;//logo mysql
    public URL pathicone = ClassLoader.getSystemResource("icone.png");

    //constructeur de la page de connection
    public PageConnexion(String titre) {
        //definition du titre de la page
        super(titre);
        //definition de la taille de la page
        this.setSize(708, 452);
        //definition de la location de la page
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.pack();
        try {
            Image img = ImageIO.read(pathicone);
            this.setIconImage(img);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //boutton de connexion
        connexion.addActionListener(e -> connection());
        //touche entrer pour se connecter
        mdp.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_ENTER){
                  connection();
                }
            }
        });
    }
    //getter pour le nom et le mot de passe pour les utilisiser dans pageAcceuil et etablir la connection
    public static String getNom(){
        return nomstring;
    }
    public static String getMdp(){
        return mdpstring;
    }
    //etablir la connexion avec la base de données mysql
    public void connection(){
        try{
            //charger le driver MySQL
            Class.forName("com.mysql.cj.jdbc.Driver");
            //creer la connection
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/PROJET_ECOLE_JAVA_SQL", nom.getText(), mdp.getText());
            //creer un etat de connection
            Statement st = con.createStatement();

            nomstring = nom.getText();
            mdpstring = mdp.getText();
            nom.setText("");
            mdp.setText("");
            //creation de la page d'acceuil
            JFrame acceuil = new PageAcceuil("Gestion d'école");
            acceuil.setVisible(true);
            //sortir de la page de connection
            dispose();
        } catch (Exception E){//en cas de nom d'utilisateur ou de mot de passe erronés
            JOptionPane.showMessageDialog(null,"Mot de passe ou nom d'utilisateur incorrect"+"\nERREUR: "+E.getMessage());
        }
    }
    //lancement de l'application
    public static void main(String[] args) {
        //definition du theme de l'application
        try {
            UIManager.setLookAndFeel( new FlatDarculaLaf() );
        } catch( Exception ex ) {
            System.err.println( "Failed to initialize LaF" );
        }
        //creation de la frame de l'application
        JFrame frame = new PageConnexion("Connexion");
        frame.setVisible(true);

    }
    //charger les images
    private ImageIcon createImage(String path){
        return new ImageIcon(java.awt.Toolkit.getDefaultToolkit().getClass().getResource(path));
    }
    private void createUIComponents() {
        URL javaurl = ClassLoader.getSystemResource("java_original_wordmark_logo_icon_146459 (1).png");
        URL mysqlurl = ClassLoader.getSystemResource("mysql_original_wordmark_logo_icon_146417 (1).png");
        label1 = new JLabel(new ImageIcon(javaurl));
        label2 = new JLabel(new ImageIcon(mysqlurl));
    }
}
