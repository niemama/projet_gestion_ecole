package main;

import java.sql.*;
public class sql {
    public static void main(String[] args) {
        try{
            //charger le driver MySQL
            Class.forName("com.mysql.jdbc.Driver");
            //creer la connection
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet_ecole_java_sql", "root","admin");
            //creer un etat de connection
            Statement st = con.createStatement();
            //creer et selectionner une requette de selection
            ResultSet res = st.executeQuery("select * from deppartement");
            //affichage et parcourt des donnees
            while (res.next()){
                System.out.println("id :"+res.getString(1)+" nom : "+res.getString(2)+" chef : "+res.getString(3));
            }
        } catch (Exception e){
            System.out.println("ERROR: "+e.getMessage());
        }
    }
}